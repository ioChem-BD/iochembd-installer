# ioChem-BD installer project

This tool is bundled inside the [ioChem-BD project](https://gitlab.com/ioChem-BD/iochem-bd) to install the platform. Among its tasks are:
  * Dump SQL scripts into the empty databases.
  * Generate a security certificate and install it on a keystore and truststore inside **ssl/** folder for the HTTPS service.
  * Replace parameter values inside the code (specially configuration files).
  * Create an administrator account to initially allow operating the platform.

To build the project as a fat jar with all dependencies inside, the following command must be run:

```shell
mvn clean compile assembly:single 
```

## Upgrading the installer tool and adding it to the ioChem-BD project

The previous command will bundle the installer tool inside the *target* folder as *installer-jar-with-dependencies.jar*. 
This jar file must be copied inside the ioChem-BD project structure, specifically as *IOCHEMBD/resources/files/installer.jar*, being *IOCHEMBD* the base of the ioChem-BD project.

