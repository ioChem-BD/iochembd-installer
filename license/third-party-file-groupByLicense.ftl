<#function artifactFormat p>
    <#if p.name?index_of('Unnamed') &gt; -1>
        <#return p.artifactId + " (" + p.groupId + ":" + p.artifactId + ":" + p.version + " - " + (p.url!"no url defined") + ")">
    <#else>
        <#return p.name + " (" + p.groupId + ":" + p.artifactId + ":" + p.version + " - " + (p.url!"no url defined") + ")">
    </#if>
</#function>

<#if licenseMap?size == 0>
The project has no dependencies.
<#else>
ioChem-BD uses third-party libraries which may be distributed under different 
licenses. We have listed all of these third party libraries and their licenses
below. This file can be regenerated at any time by simply running:
    
    mvn clean verify -Dthird.party.licenses=true

You must agree to the terms of these licenses, in addition to the ioChem-BD
source code license, in order to use this software.

---------------------------------------------------
Third party Java libraries listed by License type.

PLEASE NOTE: Some dependencies may be listed under multiple licenses if they
are dual-licensed. This is especially true of anything listed as 
"GNU General Public Library" below. 
---------------------------------------------------
    <#list licenseMap as e>
        <#assign license = e.getKey()/>
        <#assign projects = e.getValue()/>
        <#if projects?size &gt; 0>

    ${license}:

        <#list projects as project>
        * ${artifactFormat(project)}
        </#list>
        </#if>
    </#list>
</#if>
