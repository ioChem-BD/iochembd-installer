/**
 * ioChem-BD installer - ioChem-BD software installer tool.
 * Copyright © 2016 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.iochembd.installer.database;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.googlecode.lanterna.gui.Action;
import com.googlecode.lanterna.gui.Border;
import com.googlecode.lanterna.gui.Component;
import com.googlecode.lanterna.gui.Window;
import com.googlecode.lanterna.gui.component.Button;
import com.googlecode.lanterna.gui.component.CheckBox;
import com.googlecode.lanterna.gui.component.Label;
import com.googlecode.lanterna.gui.component.Panel;
import com.googlecode.lanterna.gui.component.TextBox;
import com.googlecode.lanterna.gui.dialog.MessageBox;


public class DatabaseParameters extends Window {

	private final int MIN_POSTGRESQL_VERSION = 84;	
	
	private TextBox hostnameTxt;
	private TextBox portTxt;	
	private TextBox usernameTxt;
	private TextBox passwordTxt;
	private CheckBox sslEnabled = null;

	
	static final Logger logger = LogManager.getLogger(DatabaseParameters.class.getName());
	
	public DatabaseParameters(String title) {
		super(title);		
		Panel main = new Panel(new Border.Invisible(), Panel.Orientation.HORISONTAL);		
		Panel labelPanel = new Panel(new Border.Invisible(),Panel.Orientation.VERTICAL);
		Panel textboxPanel = new Panel(new Border.Invisible(),Panel.Orientation.VERTICAL);
		Panel buttons = new Panel(new Border.Invisible(), Panel.Orientation.HORISONTAL);
		
		addComponent(new Label("Setup PostgreSQL connection parameters"));
		
		main.addComponent(labelPanel);
		main.addComponent(textboxPanel);		
					
		labelPanel.addComponent(new Label("Host:"));
		labelPanel.addComponent(new Label("Port:"));
		labelPanel.addComponent(new Label(""));
		labelPanel.addComponent(new Label("Username:"));
		labelPanel.addComponent(new Label("Password:"));		
		
		hostnameTxt = new TextBox("localhost",30);
		portTxt		= new TextBox("5432",20);
		sslEnabled = new CheckBox("Uses SSL",false);
		usernameTxt = new TextBox("iochembd",30);
		passwordTxt = new TextBox("",30);
						
		textboxPanel.addComponent(hostnameTxt);
		textboxPanel.addComponent(portTxt);
		textboxPanel.addComponent(sslEnabled);
		textboxPanel.addComponent(usernameTxt);
		textboxPanel.addComponent(passwordTxt);
		
		Button testConnectionBtn =  new Button("Test connection" , new Action(){
			@Override 
			public void doAction() {
				if(usernameTxt.getText().equals("") || passwordTxt.getText().equals("")){
					MessageBox.showMessageBox(getOwner(), "Bad credentials", "Please fill username/password with valid values.");
					return;
				}
				if(hostnameTxt.getText().equals("") || portTxt.getText().equals("")){					
					MessageBox.showMessageBox(getOwner(), "Bad database parameters", "Please fill hostname/port with valid values.");
					return;
				}
				if(testConnection(hostnameTxt.getText(), portTxt.getText(),usernameTxt.getText(),passwordTxt.getText(), sslEnabled.isSelected()) == 0)
					MessageBox.showMessageBox(getOwner(), "Test result","Connection succeeded!");
			}
		});
		testConnectionBtn.setAlignment(Component.Alignment.LEFT_CENTER);
		
		Button createDatabaseBtn = new Button("Continue" , new Action(){
			@Override 
			public void doAction() {				
				if(testConnection(hostnameTxt.getText(), portTxt.getText(), usernameTxt.getText(), passwordTxt.getText(), sslEnabled.isSelected()) == 0)
					closeWindow();				
			}
		});
		createDatabaseBtn.setAlignment(Component.Alignment.BOTTOM_RIGHT);
		
		Button cancelBtn = new Button("Cancel" , new Action(){
			@Override 
			public void doAction() {				
				System.exit(0);
			}
		});
		cancelBtn.setAlignment(Component.Alignment.BOTTOM_RIGHT);
		
		buttons.addComponent(testConnectionBtn);
		buttons.addComponent(createDatabaseBtn);
		buttons.addComponent(cancelBtn);
			
		addComponent(main);
		addComponent(buttons);		
	}
	
	private void closeWindow(){
		this.close();
	}	
	
	public String getPort(){
		return portTxt.getText().trim(); 
	}
	
	public String getHostname(){
		return hostnameTxt.getText().trim();		
	}	

	public boolean getIsSslEnabled(){
		return sslEnabled.isSelected();
	}

	public String getUsername(){
		return usernameTxt.getText().trim();		
	}	
	public String getPassword(){
		return passwordTxt.getText().trim();		
	}			
	
	private int testConnection(String hostname, String port, String username, String password, boolean hasSSL){
		String connectionUrl = "jdbc:postgresql://" + hostname + ":" + port;
		try {		
			testConnectionToDatabase(connectionUrl + "/iochemCreate", username, password, hasSSL);
			testConnectionToDatabase(connectionUrl + "/iochemBrowse", username, password, hasSSL);
			testDatabaseVersion(connectionUrl + "/iochemCreate", username, password, hasSSL);			
		} catch (SQLException e) {
			MessageBox.showMessageBox(getOwner(), "Exception", e.getMessage());
			logger.error(e.getMessage());
			return -1;
		}		
		return 0;
	}
	
	private void testConnectionToDatabase(String connectionUrl, String username, String password, boolean hasSSL) throws SQLException{
		Connection connection = null;
		if(hasSSL)
			connectionUrl += "?ssl=true&sslmode=require";
		connection = DriverManager.getConnection(connectionUrl,username, password);		
		connection.close();		
	}
	
	private int testDatabaseVersion(String connectionUrl, String username, String password, boolean hasSSL) throws SQLException {
		Connection connection = null;
		if(hasSSL)
			connectionUrl += "?ssl=true&sslmode=require";
		connection = DriverManager.getConnection(connectionUrl,username, password);
		int majorVersion = connection.getMetaData().getDatabaseMajorVersion();
		int minorVersion = connection.getMetaData().getDatabaseMinorVersion();
		int version = majorVersion * 10 + minorVersion;
		if(version < MIN_POSTGRESQL_VERSION){
			MessageBox.showMessageBox(getOwner(), "Wrong database version ", "Version v" + majorVersion +"." + minorVersion + " is not compatible with ioChem-BD software,\nplease check system requirements");
			logger.error("Version v" + majorVersion +"." + minorVersion + " is not compatible with ioChem-BD software,\nplease check system requirements");
			return -1;
		}
		//MessageBox.showMessageBox(getOwner(), "Database version ", "Version v" + majorVersion +"." + minorVersion);				
		connection.close();
		return 0;
	}

}
