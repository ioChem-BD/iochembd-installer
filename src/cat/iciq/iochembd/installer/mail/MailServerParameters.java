/**
 * ioChem-BD installer - ioChem-BD software installer tool.
 * Copyright © 2016 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.iochembd.installer.mail;

import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.googlecode.lanterna.gui.Action;
import com.googlecode.lanterna.gui.Border;
import com.googlecode.lanterna.gui.Window;
import com.googlecode.lanterna.gui.component.Button;
import com.googlecode.lanterna.gui.component.CheckBox;
import com.googlecode.lanterna.gui.component.Label;
import com.googlecode.lanterna.gui.component.Panel;
import com.googlecode.lanterna.gui.component.TextBox;
import com.googlecode.lanterna.gui.dialog.MessageBox;
import com.googlecode.lanterna.gui.dialog.TextInputDialog;

public class MailServerParameters extends Window {
	private static final String EMAIL_PATTERN ="^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
	
	private TextBox hostname = null;
	private TextBox port 	 = null;
	private CheckBox sslEnabled = null;
	private CheckBox starttlsEnabled = null;
	private TextBox mailFrom = null;
	private TextBox username = null;
	private TextBox password = null;
	
	static final Logger logger = LogManager.getLogger(MailServerParameters.class.getName());
	
	public MailServerParameters(String title) {
		super(title);
		Panel main    = new Panel(new Border.Invisible(), Panel.Orientation.HORISONTAL);
		Panel labelPanel   = new Panel(new Border.Invisible(), Panel.Orientation.VERTICAL);
		Panel textboxPanel = new Panel(new Border.Invisible(), Panel.Orientation.VERTICAL);		
		Panel buttons = new Panel(new Border.Invisible(), Panel.Orientation.HORISONTAL);
		
		labelPanel.addComponent(new Label("SMTP server hostname:"));		
		labelPanel.addComponent(new Label("SMTP server port:"));
		labelPanel.addComponent(new Label(""));
		labelPanel.addComponent(new Label(""));
		labelPanel.addComponent(new Label("Mail from default address:* "));
		labelPanel.addComponent(new Label("Account username (if required):"));
		labelPanel.addComponent(new Label("Account password (if required):"));
		
		hostname = new TextBox("", 40);
		port = new TextBox("25",40);
		sslEnabled = new CheckBox("Uses SSL",false);
		starttlsEnabled = new CheckBox("Uses STARTTLS", false);
		mailFrom = new TextBox("",40);
		username = new TextBox("",40);
		password = new TextBox("",40);		
		
		textboxPanel.addComponent(hostname);
		textboxPanel.addComponent(port);
		textboxPanel.addComponent(sslEnabled);
		textboxPanel.addComponent(starttlsEnabled);
		textboxPanel.addComponent(mailFrom);
		textboxPanel.addComponent(username);
		textboxPanel.addComponent(password);
		textboxPanel.addComponent(new Label(""));
		textboxPanel.addComponent(new Label("* Required field", 40));
		
		buttons.addComponent(new Button("Send test" , new Action(){
			@Override 
			public void doAction() {						
				sendTestEmail();
			}
		}));
		
		buttons.addComponent(new Button("Continue" , new Action(){
			@Override 
			public void doAction() {	
				if(areFieldsValid())
					closeWindow();
			}
		}));
		
		buttons.addComponent(new Button("Cancel" , new Action(){
			@Override 
			public void doAction() {						
				System.exit(0);
			}
		}));

		addComponent(new Label("SMTP mail server configuration parameters, required on user notification:"));		
		main.addComponent(labelPanel);
		main.addComponent(textboxPanel);
		addComponent(main);
		addComponent(new Label(""));			
		addComponent(buttons);
	}
	
	private boolean areFieldsValid(){
		if(mailFrom.getText().trim().equals("")){
			MessageBox.showMessageBox(getOwner(), "Empty field", "Mail-from is a required field.");
			return false;			
		} else if(!hostname.getText().isEmpty() && !hostname.getText().matches("\\b(?:[0-9]{1,3}\\.){3}[0-9]{1,3}\\b") && !hostname.getText().matches("([a-z0-9]+(-[a-z0-9]+)*\\.)+[a-z]{2,}") && !hostname.getText().matches("localhost")){
			MessageBox.showMessageBox(getOwner(), "Wrong mail server hostname", "Please define a qualified domain name or an IP.");
			return false;
		} else if(!port.getText().isEmpty() && !port.getText().trim().matches("\\b([0-9]{1,5})\\b")){
			MessageBox.showMessageBox(getOwner(), "Wrong port number", "Wrong port number.");
			return false;
		} else if(!mailFrom.getText().trim().matches(EMAIL_PATTERN)){
			MessageBox.showMessageBox(getOwner(), "Wrong email address", "Wrong email address.");
			return false;
		}				
		return true;
	}
	
	private void closeWindow(){
		close();
	}
	
	
	
	
	@SuppressWarnings("deprecation")
	private void sendTestEmail(){		
		
		String toAddress = TextInputDialog.showTextInputBox(getOwner(), "Set destination", "To addres:", "");
		if(toAddress.equals(""))
			return;
	
		HtmlEmail email = new HtmlEmail();		 
		try{
	    	email.setBounceAddress(getFromAddress());
	    	email.setHostName(getMailHostname());			
			email.setFrom(getFromAddress());
			email.addTo(toAddress);		
			email.setSubject("ioChem-BD installation test email");
			email.setAuthentication(getUsername(), getPassword());
			email.setSSLOnConnect(sslEnabled.isSelected());
			email.setStartTLSEnabled(starttlsEnabled.isSelected());
			email.setSmtpPort(Integer.parseInt(getMailPort()));
			email.setHtmlMsg("<html><body>This is a test email sent from ioChem-BD installer.<br>If you have recieved it, your SMTP parameters are correct. </body></html>");			
            email.send();
            MessageBox.showMessageBox(getOwner(), "Message sending", "\nEmail sent successfully!\n");
            logger.info("Test email sent successfully.");
        } catch (EmailException e) {
        	MessageBox.showMessageBox(getOwner(), "Error on message sending", "Please check error log file");
        	logger.error(e.getMessage());
		}        
	}
	
	public String getMailHostname(){
		return hostname.getText().trim(); 
	}
	
	public String getMailPort(){
		return port.getText().trim();		
	}
	
	public String getFromAddress(){
		return mailFrom.getText().trim();		
	}
	
	public String getUsername(){
		return username.getText().trim();		
	}
	
	public String getPassword(){
		return password.getText().trim();		
	}

	public boolean getIsSslEnabled(){
		return sslEnabled.isSelected();
	}
	
	public boolean getIsStarttlsEnabled(){
		return starttlsEnabled.isSelected();
	}
}
