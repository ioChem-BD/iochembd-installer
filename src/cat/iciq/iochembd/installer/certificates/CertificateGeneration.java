/**
 * ioChem-BD installer - ioChem-BD software installer tool.
 * Copyright © 2016 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.iochembd.installer.certificates;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.googlecode.lanterna.gui.Action;
import com.googlecode.lanterna.gui.Border;
import com.googlecode.lanterna.gui.Window;
import com.googlecode.lanterna.gui.component.Button;
import com.googlecode.lanterna.gui.component.Label;
import com.googlecode.lanterna.gui.component.Panel;
import com.googlecode.lanterna.gui.component.TextBox;
import com.googlecode.lanterna.gui.dialog.MessageBox;


public class CertificateGeneration extends Window {
		
	private TextBox hostname;
	private TextBox hostIp;
	private TextBox hostPort;
	private TextBox organizationalUnit;
	private TextBox organization;
	private TextBox city;
	private TextBox state;
	private TextBox countryCode;
	
	static final Logger logger = LogManager.getLogger(CertificateGeneration.class.getName());
	
	public CertificateGeneration(String title) {
		super(title);		
		
		Panel main    = new Panel(new Border.Invisible(), Panel.Orientation.HORISONTAL);
		Panel labelPanel   = new Panel(new Border.Invisible(), Panel.Orientation.VERTICAL);
		Panel textboxPanel = new Panel(new Border.Invisible(), Panel.Orientation.VERTICAL);		
		Panel buttons = new Panel(new Border.Invisible(), Panel.Orientation.HORISONTAL);
		
		labelPanel.addComponent(new Label("Server hostname*:"));		
		labelPanel.addComponent(new Label("Server IP*:"));
		labelPanel.addComponent(new Label("Server port*:"));
		labelPanel.addComponent(new Label("Organization*:"));
		labelPanel.addComponent(new Label("Organizational unit:"));
		labelPanel.addComponent(new Label("City/Locality:"));
		labelPanel.addComponent(new Label("State/Province:"));
		labelPanel.addComponent(new Label("Two-letter contry code:"));
		
		hostname = new TextBox("", 40);
		hostIp = new TextBox("",40);
		hostPort = new TextBox("443",40);		
		organization = new TextBox("",40);
		organizationalUnit = new TextBox("",40);
		city = new TextBox("",40);
		state = new TextBox("",40);
		countryCode = new TextBox("",3);
		
		textboxPanel.addComponent(hostname);
		textboxPanel.addComponent(hostIp);
		textboxPanel.addComponent(hostPort);		
		textboxPanel.addComponent(organization);
		textboxPanel.addComponent(organizationalUnit);
		textboxPanel.addComponent(city);
		textboxPanel.addComponent(state);
		textboxPanel.addComponent(countryCode);
		textboxPanel.addComponent(new Label("* Required fields"));
		
		buttons.addComponent(new Button("Continue" , new Action(){
			@Override 
			public void doAction() {
				if(areFieldsValid()){
					normalizePortNumber();
					closeWindow();
				}
			}
		}));

		buttons.addComponent(new Button("Cancel" , new Action(){
			@Override 
			public void doAction() {
				System.exit(0);
			}
		}));

		addComponent(new Label("Certificate generation parameters, required in HTTPS certificate generation."));		
		main.addComponent(labelPanel);
		main.addComponent(textboxPanel);
		addComponent(main);
		addComponent(new Label(""));			
		addComponent(buttons);
	}
	
	private boolean areFieldsValid(){
		if(hostname.getText().equals("") || hostIp.getText().equals("") || organization.getText().equals("")){
			MessageBox.showMessageBox(getOwner(), "Empty field", "Hostname, IP, port and Organization are required fields.");
			return false;
		}else if(!hostname.getText().matches("\\b(?:[0-9]{1,3}\\.){3}[0-9]{1,3}\\b") && !hostname.getText().matches("([a-z0-9]+(-[a-z0-9]+)*\\.)+[a-z]{2,}") && !hostname.getText().matches("localhost")){
			MessageBox.showMessageBox(getOwner(), "Wrong hostname value", "Please define a qualified domain name or an IP.");
			return false;
		}else if(!hostIp.getText().trim().matches("\\b(?:[0-9]{1,3}\\.){3}[0-9]{1,3}\\b")){
			MessageBox.showMessageBox(getOwner(), "Wrong IP address", "Wrong IP address.");
			return false;
		}else if(!hostPort.getText().trim().equals("") && !hostPort.getText().trim().matches("\\b([0-9]{1,5})\\b")){
			MessageBox.showMessageBox(getOwner(), "Wrong port value", "Wrong port number.");
			return false;
		}else if(hostPort.getText().trim().equals("80")){
			MessageBox.showMessageBox(getOwner(), "Wrong port value", "Can't use 80 as port number for HTTPS protocol, use default 443 instead.");
			return false;
		}
		
		return true;
	}
	
	private void normalizePortNumber(){
		if(hostPort.getText().trim().equals("443"))			
			hostPort.setText("");
	}
	
	private void closeWindow(){
		this.close();
	}
	
	public String getHostname(){
		return hostname.getText().trim();
	}
	
	public String getHostIp(){
		return hostIp.getText().trim();
	}
	
	public String getPort(){
		return hostPort.getText().trim();
	}
	
	public String getOrganization(){
		return organization.getText().trim();
	}
	
	public String getOrganizationUnit(){
		return organizationalUnit.getText().trim();
	}
	
	public String getCity(){
		return city.getText().trim();
	}
	
	public String getState(){
		return state.getText().trim();
	}
	
	public String getCountryCode(){
		return countryCode.getText().trim();
	}
	
}
