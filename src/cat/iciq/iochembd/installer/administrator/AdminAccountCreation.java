/**
 * ioChem-BD installer - ioChem-BD software installer tool.
 * Copyright © 2016 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.iochembd.installer.administrator;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.googlecode.lanterna.gui.Action;
import com.googlecode.lanterna.gui.Border;
import com.googlecode.lanterna.gui.Window;
import com.googlecode.lanterna.gui.component.Button;
import com.googlecode.lanterna.gui.component.Label;
import com.googlecode.lanterna.gui.component.Panel;
import com.googlecode.lanterna.gui.component.PasswordBox;
import com.googlecode.lanterna.gui.component.TextBox;
import com.googlecode.lanterna.gui.dialog.MessageBox;


public class AdminAccountCreation extends Window {
	
	private static final Logger logger = LogManager.getLogger(AdminAccountCreation.class.getName());
	private static final String EMAIL_PATTERN ="^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
	
	private TextBox email = null;
	private PasswordBox password = null;
	private PasswordBox repeatPassword = null;	
	
	public AdminAccountCreation(String title) {
		super(title);
		Panel main    = new Panel(new Border.Invisible(), Panel.Orientation.HORISONTAL);
		Panel labelPanel   = new Panel(new Border.Invisible(), Panel.Orientation.VERTICAL);
		Panel textboxPanel = new Panel(new Border.Invisible(), Panel.Orientation.VERTICAL);		
		Panel buttons = new Panel(new Border.Invisible(), Panel.Orientation.HORISONTAL);
		
		labelPanel.addComponent(new Label("Administrator email:"));		
		labelPanel.addComponent(new Label("Password:"));
		labelPanel.addComponent(new Label("Repeat password:"));
		
		email = new TextBox("", 40);
		password = new PasswordBox("",40);
		repeatPassword = new PasswordBox("",40);	
		
		textboxPanel.addComponent(email);
		textboxPanel.addComponent(password);
		textboxPanel.addComponent(repeatPassword);		
		
		buttons.addComponent(new Button("Continue" , new Action(){
			@Override 
			public void doAction() {
				if(email.getText().equals("") || !email.getText().matches(EMAIL_PATTERN)){
					MessageBox.showMessageBox(getOwner(), "Email required ", "Must provide a valid email address." );					
					return;
				}else if(password.getText().equals("") || repeatPassword.getText().equals("") || !password.getText().equals(repeatPassword.getText())){
					MessageBox.showMessageBox(getOwner(), "Incorrect password", "Password empty or mismatch." );					
					return;
				}				
				closeWindow();
			}
		}));
		buttons.addComponent(new Button("Cancel" , new Action(){
			@Override 
			public void doAction() {
				System.exit(0);
			}
		}));

		
		addComponent(new Label("Please provide system administrator credentials."));		
		main.addComponent(labelPanel);
		main.addComponent(textboxPanel);
		addComponent(main);
		addComponent(new Label(""));			
		addComponent(buttons);
	}

	private void closeWindow(){
		close();
	}
	
	public String getEmail(){
		return email.getText().trim(); 
	}
	
	public String getPassword(){
		return password.getText().trim();
	}
	
}