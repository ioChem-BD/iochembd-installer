/**
 * ioChem-BD installer - ioChem-BD software installer tool.
 * Copyright © 2016 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.iochembd.installer.helper;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.tools.ant.Project;
import org.apache.tools.ant.ProjectHelper;
import org.apache.tools.ant.util.FileUtils;

import cat.iciq.iochembd.installer.Main;

public class AntRunner {
	private static final Logger log = LogManager.getLogger(AntRunner.class.getName());
	
	public static String PROPERTIES_FILENAME = "resource.properties";
	private String basePath = "";
	
	public AntRunner(String basePath){
		this.basePath = basePath;
	}

	public void launchAntBuild() throws Exception{
		buildPropertiesFile();								
		replaceFieldsWithAnt();	
	}
	
	private void buildPropertiesFile() throws Exception {
		FileOutputStream fop = null;
		File file;
		StringBuilder content = new StringBuilder();	
				
		
		String metsInstitutionName = Main.organizationName;
		String casServerHostname = Main.hostName;
		String casServerPort = Main.hostPort;
		String casServerApp = "cas";
		String browseServerHostname = Main.hostName;
		String browseServerPort = Main.hostPort;
		String browseServerJspuiContext = "browse";
		String browseServerRestContext = "rest";
		String browseServerSwordContext = "swordv2";
		String createServerHostname = Main.hostName;
		String createServerPort = Main.hostPort;
		String createServerApp = "create";
		
		String createDatabaseHost = Main.databaseHost;
		String createDatabasePort = Main.databasePort;
		String createDatabaseName = "iochemCreate";
		String createDatabaseUser = Main.databaseUser;
		String createDatabasePwd = Main.databasePwd;
		boolean databaseIsSSL = Main.databaseIsSSL;
		
		String browseDatabaseHost = Main.databaseHost;
		String browseDatabasePort = Main.databasePort;
		String browseDatabaseUser = Main.databaseUser;
		String browseDatabasePwd = Main.databasePwd;

		String portDelimiter = Main.hostPort.equals("") ? "" : ":";		
		String jumboTemplateBaseUrl = "https://" + createServerHostname + portDelimiter + createServerPort + "/" + createServerApp + "/jumbo";
		
		String adminEmail = Main.adminEmail;
		
		String mailServer = Main.mailServer; 
		String mailServerUsername = Main.mailServerUsername;
		String mailServerPassword = Main.mailServerPassword;
		String mailServerPort = Main.mailServerPort;
		String mailFromAddress = Main.mailFromAddress;
		
		String tomcatPort = Main.hostPort.equals("")? "443": Main.hostPort ;

		content.append("${browse.home}=").append(basePath + "/browse").append("\n");
		content.append("${jumbo.template.base.url}=").append(jumboTemplateBaseUrl).append("\n");
		content.append("${mets.institution.name}=").append(metsInstitutionName).append("\n");
		content.append("${cas.server.hostname}=").append(casServerHostname).append("\n");
		content.append("${cas.server.port.delimiter}=").append(portDelimiter).append("\n");
		content.append("${cas.server.port}=").append(casServerPort).append("\n");
		content.append("${cas.server.app}=").append(casServerApp).append("\n");
		content.append("${browse.server.hostname}=").append(browseServerHostname).append("\n");
		content.append("${browse.server.port.delimiter}=").append(portDelimiter).append("\n");
		content.append("${browse.server.port}=").append(browseServerPort).append("\n");		
		content.append("${browse.server.jspui.context}=").append(browseServerJspuiContext).append("\n");
		content.append("${browse.server.rest.context}=").append(browseServerRestContext).append("\n");
		content.append("${browse.server.sword.context}=").append(browseServerSwordContext).append("\n");
		content.append("${create.server.hostname}=").append(createServerHostname).append("\n");
		content.append("${create.server.port.delimiter}=").append(portDelimiter).append("\n");
		content.append("${create.server.port}=").append(createServerPort).append("\n");
		content.append("${create.server.app}=").append(createServerApp).append("\n");				
		
		content.append("${create.database.host}=").append(createDatabaseHost).append("\n");
		content.append("${create.database.port}=").append(createDatabasePort).append("\n");
		content.append("${create.database.name}=").append(createDatabaseName).append("\n");
		content.append("${create.database.user}=").append(createDatabaseUser).append("\n");
		content.append("${create.database.pwd}=").append(createDatabasePwd).append("\n");		
		content.append("${browse.database.host}=").append(browseDatabaseHost).append("\n");
		content.append("${browse.database.port}=").append(browseDatabasePort).append("\n");
		content.append("${browse.database.user}=").append(browseDatabaseUser).append("\n");
		content.append("${browse.database.pwd}=").append(browseDatabasePwd).append("\n");
		
		content.append("${database.extra.parameters}=").append(databaseIsSSL? "?ssl=true&sslmode=require":"").append("\n");	
			
		content.append("${knowledgebase.url}=").append(Main.KNOWLEDGEBASE_URL).append("\n");
		content.append("${feedback.url}=").append(Main.FEEDBACK_URL).append("\n");
		content.append("${doi.daemon.url}=").append(Main.DOI_DAEMON_URL).append("\n");
		
		content.append("${browse.admin.email}=").append(adminEmail).append("\n");
		
		content.append("${mail.server}=").append(mailServer).append("\n"); 
		content.append("${mail.server.username}=").append(mailServerUsername).append("\n"); 
		content.append("${mail.server.password}=").append(mailServerPassword).append("\n"); 
		content.append("${mail.server.port}=").append(mailServerPort).append("\n"); 
		content.append("${mail.from.address}=").append(mailFromAddress).append("\n");
		
		content.append("${tomcat.port}=").append(tomcatPort).append("\n");
		
		content.append("${module.communication.secret}=").append(TokenManager.buildRandomString()).append("\n");
		content.append("${external.communication.secret}=").append(TokenManager.buildRandomString()).append("\n");		

		try {
			file = new File(basePath + File.separatorChar + Main.INITSCRIPT_FOLDER_NAME + File.separatorChar + PROPERTIES_FILENAME);			
			// if file exists, we'll delete it
			if (file.exists()) 
				FileUtils.delete(file);			
			file.createNewFile();
			fop = new FileOutputStream(file); 
			// get the content in bytes
			byte[] contentInBytes = content.toString().getBytes();
 
			fop.write(contentInBytes);
			fop.flush();
			fop.close();
 
		} catch (IOException e) {
			log.error(e.getMessage());
			throw new Exception("An error occurred during ant resources file generation. " + e.getMessage());			
		} finally {
			try {
				if (fop != null) {
					fop.close();
				}
			} catch (IOException e) {
				log.error(e.getMessage());
			}
		}
		
		if(Main.mailIsSSL)
			updateConfigurationWithSSL(basePath);		
	}
	
	private void updateConfigurationWithSSL(String basePath){
		try{
			String dspaceConfig = basePath + File.separatorChar + Main.BROWSE_FOLDER_NAME + File.separatorChar + "config" + File.separatorChar + "dspace.cfg";
			PropertiesConfiguration conf = new PropertiesConfiguration(dspaceConfig);
			ArrayList<String> properties = new ArrayList<String>();
			properties.add("mail.smtp.port=" + Main.mailServerPort);

			if(Main.mailServerUsername.length() > 0 && Main.mailServerPassword.length() > 0)
				properties.add("mail.smtp.auth=true");
			if(Main.mailIsSTARTTLS)
				properties.add("mail.smtp.starttls.enable=true");
			else
				properties.add("mail.smtp.ssl.enable=true");	
			
			conf.setProperty("mail.extraproperties", properties);
			conf.save();	
		}catch(ConfigurationException e){
			log.error(e.getMessage());
		}						
	}
	
	private void replaceFieldsWithAnt(){
		File buildFile = new File(basePath + File.separatorChar + Main.INITSCRIPT_FOLDER_NAME + File.separatorChar + "build.xml");
		Project p = new Project();
		p.setUserProperty("ant.file", buildFile.getAbsolutePath());
		p.init();
		ProjectHelper helper = ProjectHelper.getProjectHelper();
		p.addReference("ant.projectHelper", helper);
		helper.parse(p, buildFile);
		p.executeTarget("replaceTokens");
		p.executeTarget("buildInchiLibrary");
	}
}
