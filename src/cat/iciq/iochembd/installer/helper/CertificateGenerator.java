/**
 * ioChem-BD installer - ioChem-BD software installer tool.
 * Copyright © 2016 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.iochembd.installer.helper;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.Security;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.asn1.x500.X500NameBuilder;
import org.bouncycastle.asn1.x500.style.BCStyle;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.asn1.x509.Extension;
import org.bouncycastle.asn1.x509.GeneralName;
import org.bouncycastle.asn1.x509.GeneralNames;
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;
import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.cert.X509v3CertificateBuilder;
import org.bouncycastle.cert.jcajce.JcaX509CertificateConverter;
import org.bouncycastle.crypto.params.AsymmetricKeyParameter;
import org.bouncycastle.crypto.util.PrivateKeyFactory;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.operator.ContentSigner;
import org.bouncycastle.operator.DefaultDigestAlgorithmIdentifierFinder;
import org.bouncycastle.operator.DefaultSignatureAlgorithmIdentifierFinder;
import org.bouncycastle.operator.bc.BcRSAContentSignerBuilder;

import cat.iciq.iochembd.installer.Main;

public class CertificateGenerator {
	
	private static final String KEYSTORE_PASSWORD = "changeit";
	private static final Logger log = LogManager.getLogger(CertificateGenerator.class.getName());
	private String basePath = null;
	
	public CertificateGenerator(String basePath){
		this.basePath = basePath;
	}

	@SuppressWarnings("deprecation")
	public void buildKeystoreAndCertificate(String hostname, String hostIp, String organizationalUnit, String organization, String city, String state, String countryCode) throws Exception {		
	 	if(organizationalUnit.equals(""))
			organizationalUnit = "None";
	    if(organization.equals(""))
	    	organization = "None";
	    if(city.equals(""))
	    	city = "None";
	    if(state.equals(""))
	    	state = "None";
	    if(countryCode.equals(""))
	    	countryCode = "None";	
	    	    
		KeyStore ks = KeyStore.getInstance(KeyStore.getDefaultType());		
		char[] password = KEYSTORE_PASSWORD.toCharArray();
		ks.load(null, password);
		//Now build our self signed certificate, code taken from http://blog.thilinamb.com/2010/01/how-to-generate-self-signed.html
		String domainName = normalizeField(hostname.trim());			
		KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");  
		keyPairGenerator.initialize(2048);  
		KeyPair kPair = keyPairGenerator.generateKeyPair();
		
		X500Name name = buildName(domainName, organization, organizationalUnit, city, state, countryCode);
		X509Certificate certificate = generateCertificate("SHA512WITHRSA", name, domainName, 3650, kPair);	
        
        ks.setKeyEntry(Main.IOCHEMBD_ALIAS, kPair.getPrivate(), KEYSTORE_PASSWORD.toCharArray(), new java.security.cert.Certificate[]{certificate});        
		// Store the keystore.
        String pathToKeystore = basePath + File.separatorChar + Main.SSL_FOLDER_NAME + File.separatorChar + Main.KEYSTORE_FILE;
		FileOutputStream fos = new FileOutputStream(pathToKeystore);
		ks.store(fos, password);
		fos.close();
		
		//Add certificate to JRE trusted certificates keystore
		String pathToTruststore = basePath + File.separatorChar + Main.SSL_FOLDER_NAME + File.separatorChar + Main.TRUSTSTORE_FILE; 
		installCertificateIntoTruststore(pathToTruststore, certificate);
	}
	
	
	private X500Name buildName(String commonName, String organization, String organizationUnit, String locality, String state, String country) {
		X500NameBuilder nameBuilder = new X500NameBuilder();		
		if (!commonName.isEmpty()) {
			nameBuilder.addRDN(BCStyle.CN, commonName);
		}
		if (!organizationUnit.isEmpty()) {
			nameBuilder.addRDN(BCStyle.OU, organizationUnit);
		}
		if (!organization.isEmpty()) {
			nameBuilder.addRDN(BCStyle.O, organization);
		}
		if (!locality.isEmpty()) {
			nameBuilder.addRDN(BCStyle.L, locality);
		}
		if (!state.isEmpty()) {
			nameBuilder.addRDN(BCStyle.ST, state);
		}
		if (!country.isEmpty()) {
			nameBuilder.addRDN(BCStyle.C, country);
		}
		
		return nameBuilder.build();
	}
	
	
	
	public X509Certificate generateCertificate(String algorithm, X500Name name, String dn, int daysToExpire,  KeyPair keyPair) throws CertificateException {
	    try {
	        Security.addProvider(new BouncyCastleProvider());
	        AlgorithmIdentifier sigAlgId = new DefaultSignatureAlgorithmIdentifierFinder().find(algorithm);
	        AlgorithmIdentifier digAlgId = new DefaultDigestAlgorithmIdentifierFinder().find(sigAlgId);
	        AsymmetricKeyParameter privateKeyAsymKeyParam = PrivateKeyFactory.createKey(keyPair.getPrivate().getEncoded());
	        SubjectPublicKeyInfo subPubKeyInfo = SubjectPublicKeyInfo.getInstance(keyPair.getPublic().getEncoded());
	        ContentSigner sigGen = new BcRSAContentSignerBuilder(sigAlgId, digAlgId).build(privateKeyAsymKeyParam);
	        //X500Name name = new X500Name(dn);	       
	        Date from = new Date();
	        Date to = new Date(from.getTime() + daysToExpire * 86400000L);
	        BigInteger sn = new BigInteger(64, new SecureRandom());
	        X509v3CertificateBuilder v3CertGen = new X509v3CertificateBuilder(name, sn, from, to, name, subPubKeyInfo);

	        // If an IP is provided, set it as alternatives
	        if (dn.matches("\\b(?:[0-9]{1,3}\\.){3}[0-9]{1,3}\\b")) {
	            GeneralNames subjectAltName = new GeneralNames(new GeneralName(GeneralName.iPAddress, dn));
	            v3CertGen.addExtension(Extension.subjectAlternativeName, false, subjectAltName);	        	
	        }	            
	        X509CertificateHolder certificateHolder = v3CertGen.build(sigGen);
	        return new JcaX509CertificateConverter().setProvider("BC").getCertificate(certificateHolder);
	    } catch (CertificateException ce) {
	        throw ce;
	    } catch (Exception e) {
	        throw new CertificateException(e);
	    }
	}	

	private void installCertificateIntoTruststore(String truststorePath, Certificate certificate) throws KeyStoreException, CertificateException, NoSuchAlgorithmException, IOException{	
		FileInputStream is = new FileInputStream(truststorePath);
		KeyStore keystore = KeyStore.getInstance(KeyStore.getDefaultType());
		keystore.load(is, KEYSTORE_PASSWORD.toCharArray());		
		// Add the certificate
		keystore.setCertificateEntry(Main.IOCHEMBD_ALIAS, certificate);
		// Save the new keystore contents
		FileOutputStream out = new FileOutputStream(truststorePath);
		keystore.store(out, KEYSTORE_PASSWORD.toCharArray());
		out.close();	
	}
	
	private String normalizeField(String field){
		field = field.trim();
		field = field.replace(",", "_");
		field = field.replace("\"", "");
		field = field.replace("'", "");
		if(field.contains(" ") || field.contains("\t"))
			field = "'" + field + "'";
		return field;
	}	
	
}
