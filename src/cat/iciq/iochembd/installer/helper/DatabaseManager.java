/**
 * ioChem-BD installer - ioChem-BD software installer tool.
 * Copyright © 2016 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.iochembd.installer.helper;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.Reader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.apache.ibatis.jdbc.ScriptRunner;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class DatabaseManager {
	
	private static final Logger log = LogManager.getLogger(DatabaseManager.class.getName());
	private String basePath = null;
	private final static String IOCHEM_CREATE_SCRIPT = "iochemCreate.sql";
	private final static String IOCHEM_BROWSE_SCRIPT = "iochemBrowse.sql";
		
	public DatabaseManager(String basePath){
		this.basePath = basePath;
	}
	
	public void fillDatabases(String hostname, String port, String user, String password, boolean databaseIsSSL) throws Exception{	
		//Read script files		
		File create = new File(basePath + File.separatorChar + "init-script" + File.separatorChar + IOCHEM_CREATE_SCRIPT);
		File browse = new File(basePath + File.separatorChar + "init-script" + File.separatorChar + IOCHEM_BROWSE_SCRIPT);
		if(!create.exists() || !browse.exists())			
			throw new Exception("Some of the database initialization scripts are missing. Aborting");
		
		String connectionUrl = "jdbc:postgresql://" + hostname + ":" + port;		
		//Create iochemCreate database		
		Connection con = null;		
		try {
			con = DriverManager.getConnection(connectionUrl + "/iochemCreate" + (databaseIsSSL? "?ssl=true&sslmode=require" : ""), user, password);
			ScriptRunner sr = new ScriptRunner(con);
			sr.setAutoCommit(false);
			sr.setStopOnError(true);
			Reader reader = new BufferedReader(new FileReader(create));
			sr.setSendFullScript(true);
			sr.runScript(reader);
			sr.closeConnection();
		} catch (Exception e) {
			if (con != null) {
				try {
		            con.rollback();
		        } catch(SQLException excep) {
		        	log.error(excep.getMessage());
		        }
		    }		
			throw new Exception("Failed to Execute" + create.getPath() + ".\nPlease check error log file.");   
	    }
		
		//Create iochemBrowse database
		try {
			con = DriverManager.getConnection(connectionUrl + "/iochemBrowse"  + (databaseIsSSL? "?ssl=true&sslmode=require" : ""), user, password);
			ScriptRunner sr = new ScriptRunner(con);
			Reader reader = new BufferedReader(new FileReader(browse));
			sr.setAutoCommit(false);
			sr.setStopOnError(true);
			sr.setSendFullScript(true);
			sr.runScript(reader);
			sr.closeConnection();
		} catch (Exception e) {
			if (con != null) {
				try {
		            con.rollback();
		        } catch(SQLException excep) {		        	
		        	log.error(excep.getMessage());
		        }
		    }			
			throw new Exception("Failed to Execute" + browse.getPath() + ".\nPlease check error log file.");
		}
	}
}
