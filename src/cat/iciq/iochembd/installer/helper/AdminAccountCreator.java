/**
 * ioChem-BD installer - ioChem-BD software installer tool.
 * Copyright © 2016 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.iochembd.installer.helper;

import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bouncycastle.util.encoders.Hex;

import cat.iciq.iochembd.installer.Main;

public class AdminAccountCreator {
	private static final Logger log = LogManager.getLogger(AdminAccountCreator.class.getName());

	private static final int SALT_BYTES = 128/8; // XXX magic we want 128 bits
    private static final int HASH_ROUNDS = 1024; // XXX magic 1024 rounds
    private static final String SHA_ALGORITHM = "SHA-512";
    private static final Charset UTF_8 = Charset.forName("UTF-8"); // Should always succeed:  UTF-8 is required
	
	private String email;
	private String password;
	
	public AdminAccountCreator(String email, String password){
		this.email = email;
		this.password = password;
	}
		
	
	public String getCreateAdminSQL() throws NoSuchAlgorithmException {
		String passwordParameters[] = getSaltedPasswordParameters(password);
		return String.format(
					"UPDATE eperson set email='%s' , salt='%s', password='%s', digest_algorithm='%s' , netid='%s' where eperson_id = 1", 		
					email,
					passwordParameters[0],
					passwordParameters[1],
					passwordParameters[2],
					email);	
	}
	
	public void setAdminCredentials() throws Exception{
		String query = "UPDATE eperson set email=? , salt=?, password=?, digest_algorithm=? , netid=? where eperson_id = 1";		
		Connection con = null;
		PreparedStatement updateAdmin = null;
		try {
			String connectionUrl  = "jdbc:postgresql://" + Main.databaseHost + ":" + Main.databasePort + "/iochemBrowse" + (Main.databaseIsSSL ? "?ssl=true&sslmode=require" : "");
			con = DriverManager.getConnection(connectionUrl,Main.databaseUser, Main.databasePwd);
			con.setAutoCommit(false);
			//String encodedPassword = getMD5(password.getText());
			String passwordParameters[] = getSaltedPasswordParameters(password);
		    updateAdmin = con.prepareStatement(query);
		    updateAdmin.setString(1, email);
			updateAdmin.setString(2, passwordParameters[0]);
			updateAdmin.setString(3, passwordParameters[1]);
			updateAdmin.setString(4, passwordParameters[2]);
			updateAdmin.setString(5, email);
			updateAdmin.executeUpdate(); 
			con.commit();					
		}catch (SQLException e) {			
			if (con != null) {
				try {		
		            con.rollback();
		        } catch(SQLException es) {
		        	log.error(e.getMessage());
;		        	throw new Exception("Admin user generation. Error on database update. View error log file. " + e.getMessage());
		        }
		    }
		} catch (NoSuchAlgorithmException e) {
			if (con != null) {
				try {		
		            con.rollback();
		        } catch(SQLException es) {
		        	log.error(es.getMessage());
		        	throw new Exception("Admin user generation. Error on database update while trying to hash administrator password. View error log file. " + e.getMessage());
		        }
		    }
		}finally {	       	       
	        try {
	        	if (updateAdmin != null)
	 	        	updateAdmin.close();
				con.setAutoCommit(true);
				con.close();
			} catch (SQLException e) {				
				log.error(e.getMessage());
				throw new Exception("Admin user generation. Error on database update. View error log file. " + e.getMessage());				
			}	        
	    }		
	}
   
   private String toHex(byte[] data){
       if ((data == null) || (data.length == 0))
           return null;
       
       StringBuffer result = new StringBuffer();

       // This is far from the most efficient way to do things...
       for (int i = 0; i < data.length; i++){
           int low = (int) (data[i] & 0x0F);
           int high = (int) (data[i] & 0xF0);
           result.append(Integer.toHexString(high).substring(0, 1));
           result.append(Integer.toHexString(low));
       }
       return result.toString();
   }
	
   /**
	* This function will read a plain password and encode it using SHA-512 encryption algorithm, will generate a salt and iterate multiple times to endurance it 
	* @param password
	* @return
	* @throws NoSuchAlgorithmException 
	*/
	private String[] getSaltedPasswordParameters(String password) throws NoSuchAlgorithmException{
		String parameters[] = new String[3];     
		byte[] salt = generateSalt();
		byte[] hashedPassword = digest(salt, SHA_ALGORITHM, password);
		parameters[0] = new String(Hex.toHexString(salt));
		parameters[1] = new String(Hex.toHexString(hashedPassword));
		parameters[2] = SHA_ALGORITHM;
		return parameters;
	}
   
	/** Generate an array of random bytes. */
	private synchronized byte[] generateSalt() {				
       byte[] salt = new byte[SALT_BYTES];
       new SecureRandom().nextBytes(salt);
       return salt;
   }
	
    private byte[] digest(byte[] salt, String algorithm, String secret) throws NoSuchAlgorithmException {
        MessageDigest digester;

        if (null == secret)
            secret = "";

        // Set up a digest
        digester =  MessageDigest.getInstance(algorithm);

        // Grind up the salt with the password, yielding a hash
        if (null != salt)
            digester.update(salt);

        digester.update(secret.getBytes(UTF_8)); // Round 0

        for (int round = 1; round < HASH_ROUNDS; round++)
        {
            byte[] lastRound = digester.digest();
            digester.reset();
            digester.update(lastRound);
        }

        return digester.digest();
    }
}
