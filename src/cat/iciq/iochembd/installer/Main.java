/**
 * ioChem-BD installer - ioChem-BD software installer tool.
 * Copyright © 2016 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.iochembd.installer;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.security.NoSuchAlgorithmException;
import java.security.Security;
import java.util.Properties;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.tools.ant.util.FileUtils;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

import cat.iciq.iochembd.installer.administrator.AdminAccountCreation;
import cat.iciq.iochembd.installer.certificates.CertificateGeneration;
import cat.iciq.iochembd.installer.database.DatabaseParameters;
import cat.iciq.iochembd.installer.helper.AdminAccountCreator;
import cat.iciq.iochembd.installer.helper.AntRunner;
import cat.iciq.iochembd.installer.helper.CertificateGenerator;
import cat.iciq.iochembd.installer.helper.DatabaseManager;
import cat.iciq.iochembd.installer.mail.MailServerParameters;
import cat.iciq.iochembd.installer.messages.Resume;
import cat.iciq.iochembd.installer.messages.Welcome;

import com.googlecode.lanterna.TerminalFacade;
import com.googlecode.lanterna.gui.GUIScreen;
import com.googlecode.lanterna.gui.Theme;
import com.googlecode.lanterna.terminal.Terminal.Color;

public class Main {	
	
	public static final String FEEDBACK_URL = "https://www.iochem-bd.org/";
	public static final String KNOWLEDGEBASE_URL = "https://www.iochem-bd.org/index-introduction.jsp";
	public static final String DOI_DAEMON_URL = "https://doi.iochem-bd.org";
	
	public final static String SSL_FOLDER_NAME = "ssl";
	public final static String KEYSTORE_FILE = "keystore";
	public final static String TRUSTSTORE_FILE = "truststore";
	public final static String IOCHEMBD_ALIAS = "iochem-bd";
	
	public final static String BROWSE_FOLDER_NAME = "browse";
	public final static String POST_INSTALL_FILENAME = "postinstall.sh";
	public final static String INITSCRIPT_FOLDER_NAME = "init-script";
	public final static String LOG_PROPERTIES_FILE = "log.properties";
	public final static String CREATE_FOLDER_NAME = "webapps"+ File.separatorChar + "create" ;
	
	public static String basePath = null;
	
	public static String databasePort = "";
	public static String databaseHost = "";
	public static String databaseUser = "";
	public static String databasePwd  = "";
	public static boolean databaseIsSSL = false;
	
	public static String hostName = "";
	public static String hostIp   = "";
	public static String hostPort = "";
	public static String organizationName = "";
	public static String organizationUnit = "";
	public static String city = "";
	public static String state = "";
	public static String countryCode = "";
	
	public static String adminEmail = "";
	public static String adminPassword = "";	
	
	public static String mailServer = ""; 
	public static String mailServerUsername = "";
	public static String mailServerPassword = "";
	public static String mailServerPort = "";
	public static String mailFromAddress = "";
	public static boolean mailIsSSL = false;
	public static boolean mailIsSTARTTLS = false; 
	public static boolean skipDatabaseDump = false;	
	public static String adminCreateSQLFile = "";
	
	public static GUIScreen gui = null;
	public static Options options = null;
	
	static final Logger logger = LogManager.getLogger(Main.class.getName());

	static {
		Security.addProvider(new BouncyCastleProvider()); 		// Register BC as security provider	
	}

	public static void main(String[] args) throws URISyntaxException, ParseException, FileNotFoundException, IOException {		
		CommandLine cmd = setupCli(args);
		if(!cmd.hasOption("d")) { 	// Path is required
			new HelpFormatter().printHelp("installer", options);
			return;			
		}
		
		basePath = cmd.getOptionValue("d");
		
		if(cmd.hasOption("s"))
			skipDatabaseDump = true;
		
		if(cmd.hasOption("a"))
			adminCreateSQLFile = cmd.getOptionValue("a");
		
		if(!cmd.hasOption("c")) {			// Read from user 
			initGUI();
			int returnValue = 0;
			displayWelcomeWindow();
			do{			
				displayMailServerParametersWindow();
				displayCertificateWindow();
				displayDatabaseWindow();
				displayAdminAccountWindow();
				returnValue = displayConfirmationWindow(); 
			}while(returnValue != 0);
		}else {								// Read from file
			readParametersFromFile(cmd.getOptionValue("c"));
		}
		
		startInstallation();
	}
	

	private static CommandLine setupCli( String[] args) throws ParseException {
		options = new Options();
		options.addOption("d", "directory", true, "base directory of the ioChem-BD software files.");
		options.addOption("c", "configuration", true, "path of the file with the installation parameters, for unattended installations only.");
		options.addOption("s", "skipdb", false, "Skip database SQL dump");
		options.addOption("a", "adminsql", true, "Dump admin account SQL code to the given file");
		CommandLineParser parser = new DefaultParser();
		return parser.parse( options, args);		
	}

	private static void initGUI(){
		gui = TerminalFacade.createGUIScreen();
		gui.setTheme(new MyTheme());
		if(gui == null) {
			System.err.println("Couldn't allocate a terminal!");
		    return;
		}
		gui.getScreen().startScreen();
	}
	
	private static void displayWelcomeWindow(){
		Welcome welcome = new Welcome("Welcome");
		gui.showWindow(welcome, GUIScreen.Position.CENTER);
	}
	
	private static void displayMailServerParametersWindow() {
		MailServerParameters mailServerParameters = new MailServerParameters("Mail server configuration");
		gui.showWindow(mailServerParameters, GUIScreen.Position.CENTER);
		mailServer = mailServerParameters.getMailHostname(); 
		mailServerUsername = mailServerParameters.getUsername();
		mailServerPassword = mailServerParameters.getPassword();
		mailServerPort = mailServerParameters.getMailPort();
		mailFromAddress = mailServerParameters.getFromAddress();
		mailIsSSL = mailServerParameters.getIsSslEnabled();
		mailIsSTARTTLS = mailServerParameters.getIsStarttlsEnabled();
	}
	
	
	private static void displayCertificateWindow() {
		CertificateGeneration certificateGeneration = new CertificateGeneration("Certificate generation");
		gui.showWindow(certificateGeneration, GUIScreen.Position.CENTER);
		hostName = certificateGeneration.getHostname();
		hostIp	 = certificateGeneration.getHostIp();
		hostPort = certificateGeneration.getPort();
		organizationName  = certificateGeneration.getOrganization();
		organizationUnit = certificateGeneration.getOrganizationUnit();
		city = certificateGeneration.getCity();
		state = certificateGeneration.getState();
		countryCode = certificateGeneration.getCountryCode();	
	}
	
	private static void displayDatabaseWindow() {
		DatabaseParameters databaseParameters = new DatabaseParameters("Database parameters");
		gui.showWindow(databaseParameters, GUIScreen.Position.CENTER);
		databasePort = databaseParameters.getPort();
		databaseHost = databaseParameters.getHostname();
		databaseUser = databaseParameters.getUsername();
		databasePwd  = databaseParameters.getPassword();	
		databaseIsSSL = databaseParameters.getIsSslEnabled();
	}
	
	private static void displayAdminAccountWindow(){
		AdminAccountCreation adminCreation = new AdminAccountCreation("Setup admin account");
		gui.showWindow(adminCreation, GUIScreen.Position.CENTER);
		adminEmail = adminCreation.getEmail().trim();
		adminPassword = adminCreation.getPassword().trim();
	}
	private static int displayConfirmationWindow() {
		Resume resume = new Resume("Save configurations");
		gui.showWindow(resume, GUIScreen.Position.CENTER);
		return resume.getReturn();
	}
	
	private static void readParametersFromFile(String filePath) throws FileNotFoundException, IOException {
		// Read parameters from file but in UTF-8 encoding to avoid errors when reading the file
		Properties props = new Properties();
		props.load(new InputStreamReader(new FileInputStream(filePath), StandardCharsets.UTF_8));
		mailServer  = props.getProperty("mailServer", "smtp.foobar.com").trim();
		mailServerUsername  = props.getProperty("mailServerUsername", "username").trim();
		mailServerPassword  = props.getProperty("mailServerPassword", "password").trim();
		mailServerPort  = props.getProperty("mailServerPort", "25").trim();
		mailFromAddress  = props.getProperty("mailFromAddress", "whatever@example.com").trim();
		mailIsSSL  = Boolean.parseBoolean(props.getProperty("mailIsSSL", "false"));
		mailIsSTARTTLS  = Boolean.parseBoolean(props.getProperty("mailIsSTARTTLS", "false"));
		hostName  = props.getProperty("hostName", "test.iochem-bd.org").trim();
		hostIp  = props.getProperty("hostIp", "127.0.0.1").trim();
		hostPort  = props.getProperty("hostPort", "8443").trim();
		organizationName   = props.getProperty("organizationName", "Foo Bar organization").trim();
		organizationUnit  = props.getProperty("organizationUnit", "Foo Bar deparment").trim();
		city  = props.getProperty("city", "Springfield").trim();
		state  = props.getProperty("state", "Springfield's State").trim();
		countryCode  = props.getProperty("countryCode", "US").trim();
		databasePort  = props.getProperty("databasePort", "5432").trim();
		databaseHost  = props.getProperty("databaseHost", "localhost").trim();
		databaseUser  = props.getProperty("databaseUser", "iochembd").trim();
		databasePwd   = props.getProperty("databasePwd", "iochembd").trim();
		databaseIsSSL = Boolean.valueOf(props.getProperty("databaseIsSSL", "false").trim());
		adminEmail  = props.getProperty("adminEmail", "whatever@example.com").trim();
		adminPassword  = props.getProperty("adminPassword", "mypassword").trim();
	}
	
	
	public static void startInstallation(){
		if(gui != null)
			gui.getScreen().stopScreen();
		
		System.out.println("\n");
		System.out.println("Installing ioChem-BD, please wait.");
		try{
			if(!skipDatabaseDump)
				fillDatabases();
			buildCertificates();
			if(!adminCreateSQLFile.isEmpty())
				saveAdminCreateSQL();
			else
				setAdminPassword();
			runAntBuild();
			printResume();
		}catch(Exception e){
			logger.error(e.getMessage());
			System.out.println("An exception raised during installation process, review installation.log file.");
			System.out.println("Please remove this installation folder and unzip installation file again,");
			System.out.println("and delete databases and create them again (described on ioChem-BD wiki and in documentation.)");
			return;
		}		
	}
	
	private static void printResume() throws Exception {			
		System.out.println("Done.\t\t\t\t(5/5) ioChem-BD has been installed!");
		System.out.println("");
				
		if(Main.hostPort.equals("") || Integer.valueOf(Main.hostPort) < 1024){			
			System.out.println("Now run following file as root (or sudoer):");
			System.out.println("  " + basePath + File.separatorChar + POST_INSTALL_FILENAME);
		}		
		System.out.println("Read post-installation steps at:");
		System.out.println("  " + KNOWLEDGEBASE_URL);		
	}
	private static void fillDatabases() throws Exception{
		System.out.println("Dumping databases.\t\t(1/5)");
		DatabaseManager db = new DatabaseManager(basePath);
		db.fillDatabases(databaseHost, databasePort, databaseUser, databasePwd, databaseIsSSL);
	}
	private static void buildCertificates() throws Exception{
		System.out.println("Building certificates.\t\t(2/5)");
		CertificateGenerator cg = new CertificateGenerator(basePath);
		cg.buildKeystoreAndCertificate(hostName, hostIp, organizationUnit, organizationName, city, state, countryCode);		
	}
	
	private static void saveAdminCreateSQL() throws NoSuchAlgorithmException, IOException {
		System.out.println("Setting admin account.\t\t(3/5)");
		AdminAccountCreator ac = new AdminAccountCreator(adminEmail, adminPassword);
		File file = new File(adminCreateSQLFile);
		FileOutputStream fos = new FileOutputStream(file);		
		fos.write(ac.getCreateAdminSQL().getBytes());
		fos.close();
	}
	
	private static void setAdminPassword() throws Exception{
		System.out.println("Setting admin account.\t\t(3/5)");
		AdminAccountCreator ac = new AdminAccountCreator(adminEmail, adminPassword);
		ac.setAdminCredentials();
	}
	
	private static void runAntBuild() throws Exception{
		System.out.println("Updating files.\t\t\t(4/5) (this can take a while)");
		AntRunner ar = new AntRunner(basePath);
		ar.launchAntBuild();		
	}
}

class MyTheme extends Theme{
	public MyTheme(){
		super();
		this.setDefinition(Category.SCREEN_BACKGROUND, new Definition(Color.CYAN, Color.BLACK, true));
	}
}
