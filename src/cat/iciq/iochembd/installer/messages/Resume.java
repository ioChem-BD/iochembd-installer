/**
 * ioChem-BD installer - ioChem-BD software installer tool.
 * Copyright © 2016 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.iochembd.installer.messages;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.googlecode.lanterna.gui.Action;
import com.googlecode.lanterna.gui.Border;
import com.googlecode.lanterna.gui.Component;
import com.googlecode.lanterna.gui.Window;
import com.googlecode.lanterna.gui.component.Button;
import com.googlecode.lanterna.gui.component.Label;
import com.googlecode.lanterna.gui.component.Panel;
import com.googlecode.lanterna.gui.component.TextBox;


public class Resume extends Window {

	private final static Logger logger = LogManager.getLogger(Resume.class.getName());	
	
	private int returnValue = 0;
	
	public Resume(String title) {
		super(title);			
		addComponent(new Label("All fields fulfilled, proceed with installation?"));
		
		Panel buttons = new Panel(new Border.Invisible(), Panel.Orientation.HORISONTAL);
		buttons.setAlignment(Component.Alignment.LEFT_CENTER);		
		Button continueBtn = new Button("Install", new Action(){
			@Override 
			public void doAction() {
				returnValue = 0;
				closeWindow();	
			}
		});
		Button cancelBtn = new Button("Back" , new Action(){
			@Override 
			public void doAction() {
				returnValue = -1;
				closeWindow();	
			}
		});
		Button exitBtn = new Button("Cancel" , new Action(){
			@Override 
			public void doAction() {
				System.exit(0);
			}
		});
		
		buttons.addComponent(continueBtn);
		buttons.addComponent(cancelBtn);
		buttons.addComponent(exitBtn);
		addComponent(buttons);		
	}

	public int getReturn(){
		return returnValue;
	}
	
	private void closeWindow(){
		close();
	}
	

}
