/**
 * ioChem-BD installer - ioChem-BD software installer tool.
 * Copyright © 2016 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.iochembd.installer.messages;
import com.googlecode.lanterna.gui.Action;
import com.googlecode.lanterna.gui.Border;
import com.googlecode.lanterna.gui.Window;
import com.googlecode.lanterna.gui.component.Button;
import com.googlecode.lanterna.gui.component.Label;
import com.googlecode.lanterna.gui.component.Panel;


public class Welcome extends Window {

	public Welcome(String title) {
		super(title);
		Panel main    = new Panel(new Border.Invisible(), Panel.Orientation.VERTICAL);
		Panel buttons = new Panel(new Border.Invisible(), Panel.Orientation.HORISONTAL);		
		main.addComponent(new Label("Steps inside this installer:"));
		main.addComponent(new Label("1: Setup SMTP server parameters"));
		main.addComponent(new Label("2: Configure HTTPS certificate information"));
		main.addComponent(new Label("3: Configure PostgreSQL server connection"));
		main.addComponent(new Label("4: Fill databases"));
		main.addComponent(new Label("5: Setup administrator account"));		
		main.addComponent(new Label("6: Fill configuration files with previous information"));
		main.addComponent(new Label("7: Final Resume"));
		
		buttons.addComponent(new Button("Continue" , new Action(){
			@Override 
			public void doAction() {
				closeWindow();
			}
		}));
		buttons.addComponent(new Button("Exit" , new Action(){
			@Override 
			public void doAction() {
				System.exit(0);
			}
		}));

	
		addComponent(main);
		addComponent(buttons);
	}
	
	private void closeWindow(){
		this.close();
	}
	

}
